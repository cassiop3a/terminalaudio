import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation

FILE = "example.mp3"


def open_audio_file():
    data, sample_rate = librosa.load(FILE, sr=32000, mono=True, res_type='kaiser_best')

    D = librosa.amplitude_to_db(np.abs(librosa.stft(data)), ref=np.max)

    #img = librosa.display.specshow(D, y_axis='linear', x_axis='time', sr=sample_rate)
    # librosa.display.specshow(data, sr=sample_rate)

    D = D[1:]

    # Divide freqs in log space
    grouped_freq = []
    for i in range(10):
        band = D[2**i-1:2**(i+1)-1]
        grouped_freq.append(band)

    averaged_freqs = []

    for freq in grouped_freq:
        averaged_freqs.append(np.mean(freq, axis=0))

    print(averaged_freqs)

    fig = plt.figure(0)
    x = np.arange(0, 10)
    plt.xlim(0, 10)
    plt.ylim(-80, 0)
    bars = plt.bar(x=x, height=0, bottom=-80, align='edge')

    def anim_frame(time):
        for i, b in enumerate(bars):
            b.set_height(averaged_freqs[i][time]+80)

    animation = FuncAnimation(fig, anim_frame, interval=0.1)
    plt.show()


if __name__ == '__main__':
    open_audio_file()
